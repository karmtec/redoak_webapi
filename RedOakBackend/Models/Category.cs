﻿using System.Diagnostics.Eventing.Reader;

namespace RedOakBackend.Models
{
    public class Category
    {
        public int Id { get; set; } 
        public string? Name { get; set; }

        public bool IsLandingPage { get; set; }
    }
}
