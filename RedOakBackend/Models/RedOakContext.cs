﻿using Microsoft.EntityFrameworkCore;

namespace RedOakBackend.Models
{
    public class RedOakContext(DbContextOptions<RedOakContext> options) : DbContext(options)
    {
        public DbSet<Category> Categories { get; set; }
    }
}
